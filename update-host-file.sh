
now=$(date +%s)
echo $now > updated

### Download hosts
wget https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts -O host-files/base1
wget https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts -O host-files/base2
wget https://raw.githubusercontent.com/EnergizedProtection/block/master/basic/formats/hosts -O host-files/base3
wget https://raw.githubusercontent.com/EnergizedProtection/block/master/blu/formats/hosts -O host-files/base4
wget https://raw.githubusercontent.com/EnergizedProtection/block/master/bluGo/formats/hosts -O host-files/base5
wget https://raw.githubusercontent.com/EnergizedProtection/block/master/spark/formats/hosts -O host-files/base6
#wget https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt -O host-files/base7
#wget http://winhelp2002.mvps.org/hosts.txt -O host-files/base8
#wget https://adaway.org/hosts.txt -O host-files/base9
#wget https://someonewhocares.org/hosts/zero/hosts -O host-files/base10
#wget https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext -O host-files/base11
#wget https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts -O host-files/base12
#wget https://zerodot1.gitlab.io/CoinBlockerLists/hosts -O host-files/base13
echo "downloaded all base lists"

cat host-files/hosts.01-ubuntu-default > host-files/1 && cat host-files/base1 >> host-files/1
cat host-files/hosts.01-ubuntu-default > host-files/2 && cat host-files/base2 >> host-files/2
cat host-files/hosts.01-ubuntu-default > host-files/3 && cat host-files/base3 >> host-files/3
cat host-files/hosts.01-ubuntu-default > host-files/4 && cat host-files/base4 >> host-files/4
cat host-files/hosts.01-ubuntu-default > host-files/5 && cat host-files/base5 >> host-files/5
cat host-files/hosts.01-ubuntu-default > host-files/6 && cat host-files/base6 >> host-files/6

cat host-files/hosts.01-ubuntu-default > host-files/1-2
cat host-files/hosts.01-ubuntu-default > host-files/1-3
cat host-files/hosts.01-ubuntu-default > host-files/1-4
cat host-files/hosts.01-ubuntu-default > host-files/1-5
cat host-files/hosts.01-ubuntu-default > host-files/1-6
cat host-files/hosts.01-ubuntu-default > host-files/2-3
cat host-files/hosts.01-ubuntu-default > host-files/2-4
cat host-files/hosts.01-ubuntu-default > host-files/2-5
cat host-files/hosts.01-ubuntu-default > host-files/2-6
cat host-files/hosts.01-ubuntu-default > host-files/3-4
cat host-files/hosts.01-ubuntu-default > host-files/3-5
cat host-files/hosts.01-ubuntu-default > host-files/3-6
cat host-files/hosts.01-ubuntu-default > host-files/4-5
cat host-files/hosts.01-ubuntu-default > host-files/4-6
cat host-files/hosts.01-ubuntu-default > host-files/5-6

cat host-files/hosts.01-ubuntu-default > host-files/1-2-3
cat host-files/hosts.01-ubuntu-default > host-files/1-2-4
cat host-files/hosts.01-ubuntu-default > host-files/1-2-5
cat host-files/hosts.01-ubuntu-default > host-files/1-2-6
cat host-files/hosts.01-ubuntu-default > host-files/1-3-4
cat host-files/hosts.01-ubuntu-default > host-files/1-3-5
cat host-files/hosts.01-ubuntu-default > host-files/1-3-6
cat host-files/hosts.01-ubuntu-default > host-files/1-4-5
cat host-files/hosts.01-ubuntu-default > host-files/1-4-6
cat host-files/hosts.01-ubuntu-default > host-files/1-5-6
cat host-files/hosts.01-ubuntu-default > host-files/2-3-4
cat host-files/hosts.01-ubuntu-default > host-files/2-3-5
cat host-files/hosts.01-ubuntu-default > host-files/2-3-6
cat host-files/hosts.01-ubuntu-default > host-files/2-4-5
cat host-files/hosts.01-ubuntu-default > host-files/2-4-6
cat host-files/hosts.01-ubuntu-default > host-files/2-5-6
cat host-files/hosts.01-ubuntu-default > host-files/3-4-5
cat host-files/hosts.01-ubuntu-default > host-files/3-4-6
cat host-files/hosts.01-ubuntu-default > host-files/3-5-6

cat host-files/hosts.01-ubuntu-default > host-files/1-2-3-4
cat host-files/hosts.01-ubuntu-default > host-files/1-2-4-5
cat host-files/hosts.01-ubuntu-default > host-files/1-2-4-6
cat host-files/hosts.01-ubuntu-default > host-files/1-2-5-6
cat host-files/hosts.01-ubuntu-default > host-files/1-3-4-5
cat host-files/hosts.01-ubuntu-default > host-files/1-3-4-6
cat host-files/hosts.01-ubuntu-default > host-files/1-3-5-6
cat host-files/hosts.01-ubuntu-default > host-files/1-4-5-6
cat host-files/hosts.01-ubuntu-default > host-files/2-3-4-5
cat host-files/hosts.01-ubuntu-default > host-files/2-3-4-6
cat host-files/hosts.01-ubuntu-default > host-files/2-4-5-6
cat host-files/hosts.01-ubuntu-default > host-files/3-4-5-6

cat host-files/hosts.01-ubuntu-default > host-files/1-2-3-4-5
cat host-files/hosts.01-ubuntu-default > host-files/1-2-3-4-6
cat host-files/hosts.01-ubuntu-default > host-files/1-2-3-5-6
cat host-files/hosts.01-ubuntu-default > host-files/1-2-4-5-6
cat host-files/hosts.01-ubuntu-default > host-files/1-3-4-5-6
cat host-files/hosts.01-ubuntu-default > host-files/2-3-4-5-6

cat host-files/hosts.01-ubuntu-default > host-files/1-2-3-4-5-6

sort -u host-files/base1 host-files/base2 >> host-files/1-2
sort -u host-files/base1 host-files/base3 >> host-files/1-3
sort -u host-files/base1 host-files/base4 >> host-files/1-4
sort -u host-files/base1 host-files/base5 >> host-files/1-5
sort -u host-files/base1 host-files/base6 >> host-files/1-6
sort -u host-files/base2 host-files/base3 >> host-files/2-3
sort -u host-files/base2 host-files/base4 >> host-files/2-4
sort -u host-files/base2 host-files/base5 >> host-files/2-5
sort -u host-files/base2 host-files/base6 >> host-files/2-6
sort -u host-files/base3 host-files/base4 >> host-files/3-4
sort -u host-files/base3 host-files/base5 >> host-files/3-5
sort -u host-files/base3 host-files/base6 >> host-files/3-6
sort -u host-files/base4 host-files/base5 >> host-files/4-5
sort -u host-files/base4 host-files/base6 >> host-files/4-6
sort -u host-files/base5 host-files/base6 >> host-files/5-6

sort -u host-files/base1 host-files/base4 host-files/base3 >> host-files/1-2-3
sort -u host-files/base1 host-files/base2 host-files/base4 >> host-files/1-2-4
sort -u host-files/base1 host-files/base2 host-files/base5 >> host-files/1-2-5
sort -u host-files/base1 host-files/base2 host-files/base6 >> host-files/1-2-6
sort -u host-files/base1 host-files/base3 host-files/base4 >> host-files/1-3-4
sort -u host-files/base1 host-files/base3 host-files/base5 >> host-files/1-3-5
sort -u host-files/base1 host-files/base3 host-files/base6 >> host-files/1-3-6
sort -u host-files/base1 host-files/base4 host-files/base5 >> host-files/1-4-5
sort -u host-files/base1 host-files/base4 host-files/base6 >> host-files/1-4-6
sort -u host-files/base1 host-files/base5 host-files/base6 >> host-files/1-5-6
sort -u host-files/base2 host-files/base3 host-files/base4 >> host-files/2-3-4
sort -u host-files/base2 host-files/base3 host-files/base5 >> host-files/2-3-5
sort -u host-files/base2 host-files/base3 host-files/base6 >> host-files/2-3-6
sort -u host-files/base2 host-files/base4 host-files/base5 >> host-files/2-4-5
sort -u host-files/base2 host-files/base4 host-files/base6 >> host-files/2-4-6
sort -u host-files/base2 host-files/base5 host-files/base6 >> host-files/2-5-6
sort -u host-files/base3 host-files/base4 host-files/base5 >> host-files/3-4-5
sort -u host-files/base3 host-files/base4 host-files/base6 >> host-files/3-4-6
sort -u host-files/base3 host-files/base5 host-files/base6 >> host-files/3-5-6

sort -u host-files/base1 host-files/base2 host-files/base3 host-files/base4 >> host-files/1-2-3-4
sort -u host-files/base1 host-files/base2 host-files/base4 host-files/base5 >> host-files/1-2-4-5
sort -u host-files/base1 host-files/base2 host-files/base4 host-files/base6 >> host-files/1-2-4-6
sort -u host-files/base1 host-files/base2 host-files/base5 host-files/base6 >> host-files/1-2-5-6
sort -u host-files/base1 host-files/base3 host-files/base4 host-files/base5 >> host-files/1-3-4-5
sort -u host-files/base1 host-files/base3 host-files/base4 host-files/base6 >> host-files/1-3-4-6
sort -u host-files/base1 host-files/base3 host-files/base5 host-files/base6 >> host-files/1-3-5-6
sort -u host-files/base1 host-files/base4 host-files/base5 host-files/base6 >> host-files/1-4-5-6
sort -u host-files/base2 host-files/base3 host-files/base4 host-files/base5 >> host-files/2-3-4-5
sort -u host-files/base2 host-files/base3 host-files/base4 host-files/base6 >> host-files/2-3-4-6
sort -u host-files/base2 host-files/base4 host-files/base5 host-files/base6 >> host-files/2-4-5-6
sort -u host-files/base3 host-files/base4 host-files/base5 host-files/base6 >> host-files/3-4-5-6

sort -u host-files/base1 host-files/base2 host-files/base3 host-files/base4 host-files/base5 >> host-files/1-2-3-4-5
sort -u host-files/base1 host-files/base2 host-files/base3 host-files/base4 host-files/base6 >> host-files/1-2-3-4-6
sort -u host-files/base1 host-files/base2 host-files/base3 host-files/base5 host-files/base6 >> host-files/1-2-3-5-6
sort -u host-files/base1 host-files/base2 host-files/base4 host-files/base5 host-files/base6 >> host-files/1-2-4-5-6
sort -u host-files/base1 host-files/base3 host-files/base4 host-files/base5 host-files/base6 >> host-files/1-3-4-5-6
sort -u host-files/base2 host-files/base3 host-files/base4 host-files/base5 host-files/base6 >> host-files/2-3-4-5-6

sort -u host-files/base1 host-files/base2 host-files/base3 host-files/base4 host-files/base5 host-files/base6 >> host-files/1-2-3-4-5-6

rm host-files/base1
rm host-files/base2
rm host-files/base3
rm host-files/base4
rm host-files/base5
rm host-files/base6

echo "every list has been updated"
