import QtQuick 2.9
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import UAdBlock 1.0

Page {
    id: blockPage
    header: PageHeader {
        title: i18n.tr("Blocklists")
    } 

    Flickable {
        anchors.fill: parent
        visible: !aIndicator.visible
        contentHeight: configuration.childrenRect.height

        Column {
            id: configuration
            anchors.fill: parent

            ListItem.SingleValue {
            }
            ListItem.Standard {
                text: "StevenBlack Unified"
                control: CheckBox {
                    id: enablestevenBlockUnified
                    checked: settings.stevenBlockUnified
                    onClicked: {
                        if(settings.stevenBlockUnified)
                            settings.stevenBlockUnified = false
                        else
                            settings.stevenBlockUnified = true
                    }
                }
            }
            ListItem.Standard {
                text: "StevenBlack Unified + Porno"
                control: CheckBox {
                    id: enablestevenBlockUnifiedPorno
                    checked: settings.stevenBlockUnifiedPorno
                    onClicked: {
                        if(settings.stevenBlockUnifiedPorno)
                            settings.stevenBlockUnifiedPorno = false
                        else
                            settings.stevenBlockUnifiedPorno = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Basic"
                control: CheckBox {
                    id: enableenergizedBasic
                    checked: settings.energizedBasic
                    onClicked: {
                        if(settings.energizedBasic)
                            settings.energizedBasic = false
                        else
                            settings.energizedBasic = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Blu"
                control: CheckBox {
                    id: enableenergizedBlu
                    checked: settings.energizedBlu
                    onClicked: {
                        if(settings.energizedBlu)
                            settings.energizedBlu = false
                        else
                            settings.energizedBlu = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Blu Go"
                control: CheckBox {
                    id: enableenergizedBluGo
                    checked: settings.energizedBluGo
                    onClicked: {
                        if(settings.energizedBluGo)
                            settings.energizedBluGo = false
                        else
                            settings.energizedBluGo = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Spark"
                control: CheckBox {
                    id: enableenergizedSpark
                    checked: settings.energizedSpark
                    onClicked: {
                        if(settings.energizedSpark)
                            settings.energizedSpark = false
                        else
                            settings.energizedSpark = true
                    }
                }
            }
         
              /*
            ListItem.Standard {
                text: "GoodBye Ads by Jerryn70"
                control: CheckBox {
                    id: enablegoodbyeAds
                    checked: settings.goodbyeAds
                    onClicked: {
                        if(settings.goodbyeAds)
                            settings.goodbyeAds = false
                        else
                            settings.goodbyeAds = true
                    }
                }
            }
            ListItem.Standard {
                text: "MVPS"
                control: CheckBox {
                    id: enablemvps
                    checked: settings.mvps
                    onClicked: {
                        if(settings.mvps)
                            settings.mvps = false
                        else
                            settings.mvps = true
                    }
                }
            }
            ListItem.Standard {
                text: "Adaway"
                control: CheckBox {
                    id: enableadaway
                    checked: settings.adaway
                    onClicked: {
                        if(settings.adaway)
                            settings.adaway = false
                        else
                            settings.adaway = true
                    }
                }
            }
            ListItem.Standard {
                text: "dan pollock's hosts"
                control: CheckBox {
                    id: enabledanpollocks
                    checked: settings.danpollocks
                    onClicked: {
                        if(settings.danpollocks)
                            settings.danpollocks = false
                        else
                            settings.danpollocks = true
                    }
                }
            }
            ListItem.Standard {
                text: "Peter Lowe hosts"
                control: CheckBox {
                    id: enablepeterlowes
                    checked: settings.peterlowes
                    onClicked: {
                        if(settings.peterlowes)
                            settings.peterlowes = false
                        else
                            settings.peterlowes = true
                    }
                }
            }
            ListItem.Standard {
                text: "StevenBlack Social Hosts"
                control: CheckBox {
                    id: enablesocialhosts
                    checked: settings.socialhosts
                    onClicked: {
                        if(settings.socialhosts)
                            settings.socialhosts = false
                        else
                            settings.socialhosts = true
                    }
                }
            }
            ListItem.Standard {
                text: "CoinBlockLists - by ZeroDot1"
                control: CheckBox {
                    id: enablecoinblocker
                    checked: settings.coinblocker
                    onClicked: {
                        if(settings.coinblocker)
                            settings.coinblocker = false
                        else
                            settings.coinblocker = true
                    }
                }
            }*/
        }
    }    
}
