import QtQuick 2.9
import QtQuick.Layouts 1.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.PushNotifications 0.1
import UAdBlock 1.0

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "uadblock.mariogrip"

    width: units.gu(100)
    height: units.gu(75)

    PushClient {
        id: pushClient
        appId: "uadblock.mariogrip_uAdBlock"
        onTokenChanged: console.log("👍", pushClient.token)
    }

    Settings {
        id: settings
        property string lastUpdate: "Never"
        property bool firstStart: true
        property bool checkUpdates: false
        property bool pushNotifications: true
        property bool systemStatusRW: false
        property bool uAdBlockenabled: false
        property bool stevenBlockUnified: false
        property bool stevenBlockUnifiedPorno: false
        property bool energizedBasic: false
        property bool energizedBlu: false
        property bool energizedBluGo: false
        property bool energizedSpark: false
        property bool goodbyeAds: false
        property bool mvps: false
        property bool adaway: false
        property bool danpollocks: false
        property bool peterlowes: false
        property bool socialhosts: false
        property bool coinblocker: false

    }

    Component.onCompleted: {
      console.log("Started app.")
      if(settings.checkUpdates)
      {
        checkForNewVersion(function(newVersion) {
            if (newVersion > settings.lastUpdate)
              block()
            else
              PopupUtils.open(nonewVersionPopup)
        })       
      }
    }

    property var baselists: 'https://gitlab.com/uadblock/uadblock/raw/master/host-files/'
    property var untouchedFile: 'https://gitlab.com/uadblock/uadblock/raw/master/host-files/hosts.01-ubuntu-default'
    property var updateFile: "https://gitlab.com/uadblock/uadblock/raw/master/updated"
    property var target: '/etc/hosts'
    property var backup: '/etc/hosts.backup'
    property var hostlink: '' 

    property var cmdList: []

    function checkForNewVersion(cb)
    {
        aIndicator.visible = true;
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var newVersion = xhr.responseText;
                aIndicator.visible = false;
                cb(newVersion);
            }
        }
        xhr.open('GET', updateFile, true);
        xhr.send(null);
    }


    function nextCmd(){
        if (cmdList.length == 0)
            return done();
        var next = cmdList.shift()
        console.log("next", next)
        cmd.sudo(next)
    }

    function done(){
        if(hostlink != '')
        {
          settings.lastUpdate = Date.now()/1000
          lastUpdated.value = timeConverter(settings.lastUpdate)
          hostlink = ''          
        }
        aIndicator.visible = false;
    }

    function sudo(cmd){
        cmdList.push(cmd)
    }

    function mount(){
        sudo("mount -o rw,remount /")
    }

    function umount(){
        sudo("mount -o r,remount /")
    }

    function deleteOldFiles(){

      aIndicator.visible = true

      mount()

      if(cmd.fileExists("/etc/hosts.blocklist"))
        sudo("rm /etc/hosts.blocklist")
      if(cmd.fileExists("/etc/hosts.blocklist-enabled"))
        sudo("rm /etc/hosts.blocklist-enabled")
      if(cmd.fileExists("/etc/hosts.stevenblack-enabled"))
        sudo("rm /etc/hosts.stevenblack-enabled")
      if(cmd.fileExists("/etc/hosts.energized-enabled"))
        sudo("rm /etc/hosts.energized-enabled")
      if(cmd.fileExists("/etc/hosts.adguard-enabled"))
        sudo("rm /etc/hosts.adguard-enabled")
      if(cmd.fileExists("/etc/hosts.blockFakenews-enabled"))
        sudo("rm /etc/hosts.blockFakenews-enabled")
      if(cmd.fileExists("/etc/hosts.blockGambling-enabled"))
        sudo("rm /etc/hosts.blockGambling-enabled")
      if(cmd.fileExists("/etc/hosts.blockPorn-enabled"))
        sudo("rm /etc/hosts.blockPorn-enabled")
      if(cmd.fileExists("/etc/hosts.blockSocial-enabled"))
        sudo("rm /etc/hosts.blockSocial-enabled")
      if(cmd.fileExists("/etc/hosts.without-adblock"))
        sudo("rm /etc/hosts.without-adblock")

      sudo("wget " + untouchedFile + " -O " + target)
      settings.firstStart = false

      if(!settings.systemStatusRW)
        umount()

      nextCmd() 
    }

    function block(){
        aIndicator.visible = true

        mount()

         if(settings.stevenBlockUnified)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "1"
        } 

         if(settings.stevenBlockUnifiedPorno)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "2"
        }           

         if(settings.energizedBasic)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "3"
        }   

         if(settings.energizedBlu)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "4"
        } 

         if(settings.energizedBluGo)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "5"
        } 

         if(settings.energizedSpark)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "6"
        } 
/*
         if(settings.goodbyeAds)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "7"
        } 

         if(settings.mvps)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "8"
        }           

         if(settings.adaway)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "9"
        }   

         if(settings.danpollocks)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "10"
        } 

         if(settings.peterlowes)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "11"
        } 

         if(settings.socialhosts)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "12"
        } 

         if(settings.coinblocker)
        {
          if(hostlink != "")
            hostlink = hostlink + "-"

          hostlink = hostlink + "13"
        }*/

        if(hostlink == '')
        {
          PopupUtils.open(hostlistEmptyPopup)
          settings.uAdBlockenabled = true
          done()        
        }
        else
        {
          sudo("wget " + baselists + hostlink + " -O " + target)
          sudo("wget " + untouchedFile + " -O " + backup) 
        }

        if(!settings.systemStatusRW)
          umount()

        nextCmd()
    }

    function unblock(){
      aIndicator.visible = true;
      settings.stevenBlockUnified = false
      settings.stevenBlockUnifiedPorno = false
      settings.energizedBasic = false
      settings.energizedBlu = false
      settings.energizedBluGo = false
      settings.energizedSpark = false
      settings.goodbyeAds = false
      settings.mvps = false
      settings.adaway = false
      settings.danpollocks = false
      settings.peterlowes = false
      settings.socialhosts = false
      settings.coinblocker = false
      settings.uAdBlockenabled = false
      mount()

      sudo("cp " + backup +" " + target)

      PopupUtils.open(unblockPopup)
      
    if(!settings.systemStatusRW)
      umount()
      
      nextCmd()
    }

    function timeConverter(UNIX_timestamp){
      function ii(i, len) {
          var s = i + "";
          len = len || 2;
          while (s.length < len) s = "0" + s;
          return s;
      }
      if (!(new Date(UNIX_timestamp * 1000)).getTime() > 0)
          return "Never"
      var a = new Date(UNIX_timestamp * 1000);
      var months = [i18n.tr('Jan'), i18n.tr('Feb'), i18n.tr('Mar'), i18n.tr('Apr'), i18n.tr('May'), i18n.tr('Jun'), i18n.tr('Jul'), i18n.tr('Aug'), i18n.tr('Sep'), i18n.tr('Oct'), i18n.tr('Nov'), i18n.tr('Dec')];
      var year = a.getFullYear();
      var month = months[a.getMonth()];
      var date = a.getDate();
      var hour = ii(a.getHours());
      var min = ii(a.getMinutes());
      var sec = ii(a.getSeconds());
      var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
      return time;
    }

    function countLists(){
      var count = 0
      count+= (settings.stevenBlockUnified ? 1:0)
      count+= (settings.stevenBlockUnifiedPorno ? 1:0)
      count+= (settings.energizedBasic ? 1:0)
      count+= (settings.energizedBlu ? 1:0)
      count+= (settings.energizedBluGo ? 1:0)
      count+= (settings.energizedSpark ? 1:0)
      count+= (settings.goodbyeAds ? 1:0)
      count+= (settings.mvps ? 1:0)
      count+= (settings.adaway ? 1:0)
      count+= (settings.danpollocks ? 1:0)
      count+= (settings.peterlowes ? 1:0)
      count+= (settings.socialhosts ? 1:0)
      count+= (settings.coinblocker ? 1:0)

      return count
    }

    PageStack {
        id: pageStack
        Component.onCompleted: {
          if(settings.firstStart)
          {
            var popupOldVersion = PopupUtils.open(oldVersion)
            popupOldVersion.accepted.connect(function(password) {
              deleteOldFiles()
              push(blockPage)
            })          
          }
          else
          {
            push(blockPage)
          }
        }

      Page {
        id: blockPage
        header: PageHeader {
            title: i18n.tr("uAdBlock")
            trailingActionBar {
             actions: [
              Action {
                iconName: "settings"
                text: "settings"

                onTriggered: pageStack.push(Qt.resolvedUrl("Settings.qml"))
              },
              Action {
                iconName: "like"
                text: "donate"

                onTriggered: Qt.openUrlExternally("https://liberapay.com/beli3ver")
              },
              Action {
                iconName: "help"
                text: "help"

                onTriggered: pageStack.push(Qt.resolvedUrl("Help.qml"))
              }
            ]
          }
        }

          ActivityIndicator {
            id: aIndicator
            opacity: visible ? 1 : 0
            visible: false
            running: visible
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
          }
          Rectangle {
              height: units.gu(24)
          }
          Flickable {
              anchors.fill: parent
              visible: !aIndicator.visible
              contentHeight: configuration.childrenRect.height

              Column {
                  id: configuration
                  anchors.fill: parent

                  ListItem.SingleValue {
                  }
                  ListItem.SingleValue {
                      objectName: "WarningItem"
                      height: warningColumn.childrenRect.height + units.gu(2)

                      Column {
                          anchors.fill: parent
                          anchors.topMargin: units.gu(1)

                          id: warningColumn
                          spacing: units.gu(2)
                          Icon {
                              id: warnIcon
                              width: parent.width/4
                              height: width
                              name: "security-alert"
                              anchors.horizontalCenter: parent.horizontalCenter
                          }
                          Label {
                              id: warnText
                              width: parent.width
                              horizontalAlignment: Text.AlignHCenter
                              wrapMode: Text.WordWrap
                              text: i18n.tr("Please note that this app will modify your readonly filesystem")
                          }
                      }
                  }
                  ListItem.SingleValue {
                      text: i18n.tr("Blocklists")
                      enabled: true
                      value: "<b>" + countLists() + "</b> >"
                      onClicked: pageStack.push(Qt.resolvedUrl("Lists.qml"))
                  }
                  ListItem.SingleValue {
                      id: lastUpdated
                      enabled: true
                      objectName: "lastUpdate"
                      text: i18n.tr("Last updated")
                      value: {
                          return timeConverter(settings.lastUpdate)
                      }
                  }
                  ColumnLayout{
                    height: units.gu(5)
                    width: parent.width
                    visible: !settings.uAdBlockenabled
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Button {
                        text: i18n.tr("Activate uAdBlock")
                        Layout.preferredWidth: units.gu(50)
                        Layout.alignment: Qt.AlignHCenter
                        color: UbuntuColors.green
                        onClicked: {
                            block()
                        }
                    }
                  }
                  ColumnLayout{
                    height: units.gu(5)
                    width: parent.width
                    visible: settings.uAdBlockenabled
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Button {
                        text: i18n.tr("Update uAdBlock")
                        Layout.preferredWidth: units.gu(50)
                        Layout.alignment: Qt.AlignHCenter
                        color: UbuntuColors.green
                        onClicked: {
                          checkForNewVersion(function(newVersion) {
                              if (newVersion > settings.lastUpdate)
                                block()
                              else
                                PopupUtils.open(nonewVersionPopup)
                          })
                        }
                    }
                  }
                  ColumnLayout{
                    width: parent.width
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Button {
                        text: i18n.tr("Disable uAdBlock")
                        Layout.preferredWidth: units.gu(50)
                        Layout.alignment: Qt.AlignHCenter
                        onClicked: {
                            unblock()
                        }
                    }
                  }              
              }
          }


          Cmd {
              id: cmd

              onFinished: {
                  if (!success){
                      cmdList = []
                      nextCmd();
                      error.visible = true;
                      errorLog.visible = true;
                      errorLog.text = stdout
                      return;
                  }

                  if (!busy)
                      nextCmd()
              }

              onPasswordRequested: {
                  var popup = PopupUtils.open(passwordPopup)
                  popup.accepted.connect(function(password) {
                      cmd.providePassword(password);
                  })
                  popup.rejected.connect(function() {
                      cmdList = []
                      nextCmd();
                      cmd.cancel();
                  })
              }
          }

          Component {
              id: hostlistEmptyPopup
              Dialog {
                  id: hostlistEmptDialog
                  title: i18n.tr("No lists selected!")
                  text: i18n.tr("Please, select a block list to start!")

                  signal accepted()

                  Button {
                      text: i18n.tr("Yes")
                      color: UbuntuColors.blue
                      onClicked: {
                          hostlistEmptDialog.accepted()
                          PopupUtils.close(hostlistEmptDialog)
                      }
                  }
              }
          }

          Component {
              id: nonewVersionPopup
              Dialog {
                  id: nonewVersionDialog
                  title: i18n.tr("No Update!")
                  text: i18n.tr("No update available.")

                  signal accepted()

                  Button {
                      text: i18n.tr("OK")
                      color: UbuntuColors.blue
                      onClicked: {
                          nonewVersionDialog.accepted()
                          PopupUtils.close(nonewVersionDialog)
                      }
                  }
              }
          }

          Component {
              id: oldVersion
              Dialog {
                  id: oldVersionDialog
                  title: i18n.tr("Welcome to uAdBlock 2.0")
                  text: i18n.tr("You start this app for the first time. We will look for files from the old version and make a full clean up.")

                  signal accepted()

                  Button {
                      text: i18n.tr("OK")
                      color: UbuntuColors.blue
                      onClicked: {
                          oldVersionDialog.accepted()
                          PopupUtils.close(oldVersionDialog)
                      }
                  }
              }
          }

          Component {
              id: unblockPopup
              Dialog {
                  id: unblockDialog
                  title: i18n.tr("Unblock!")
                  text: i18n.tr("uAdBlock has been deactivated!")

                  signal accepted()

                  Button {
                      text: i18n.tr("OK")
                      color: UbuntuColors.blue
                      onClicked: {
                          unblockDialog.accepted()
                          PopupUtils.close(unblockDialog)
                      }
                  }
              }
          }

          Component {
              id: passwordPopup
              Dialog {
                  id: passwordDialog
                  title: i18n.tr("Enter password")
                  text: i18n.tr("Your password is required for this action:")

                  signal accepted(string password)
                  signal rejected()

                  TextField {
                      id: passwordTextField
                      echoMode: TextInput.Password
                  }
                  Button {
                      text: i18n.tr("OK")
                      color: UbuntuColors.green
                      onClicked: {
                          passwordDialog.accepted(passwordTextField.text)
                          PopupUtils.close(passwordDialog)
                      }
                  }
                  Button {
                      text: i18n.tr("Cancel")
                      onClicked: {
                          passwordDialog.rejected();
                          PopupUtils.close(passwordDialog)
                      }
                  }
              }
      }
    }
  }

}
