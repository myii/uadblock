import QtQuick 2.9
import QtQuick.Controls 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
  id: infoPage

  header: PageHeader {
      id: listHeader
      title: i18n.tr("Help")
      flickable: flickable
  }
  Flickable {
    id: flickable
    contentHeight: dataColumn.childrenRect.height
    anchors.fill: parent

    Column {
      id: dataColumn
      anchors.fill: parent
      ListItem.SingleValue {
          objectName: "InfoItem"
          height: helpColumn.childrenRect.height + units.gu(2)

          Column {
              anchors.fill: parent
              anchors.topMargin: units.gu(1)

              id: helpColumn
              spacing: units.gu(2)
              Icon {
                  id: warnIcon
                  width: parent.width/4
                  height: width
                  name: "help"
                  anchors.horizontalCenter: parent.horizontalCenter
              }
          }
      }
      ListItem.SingleValue {
      height: whatColumn.childrenRect.height + units.gu(2)
      Column {
        id: whatColumn
        anchors.fill: parent
        anchors.topMargin: units.gu(1)
        spacing: units.gu(2)
        Label {
          width: parent.width
          wrapMode: Text.Wrap
          horizontalAlignment: Text.AlignLeft
          text: i18n.tr("<b>What is uAdBlock and how it works?</b><p>uAdBlock is an adblocker based on host lists. These lists are downloaded and anchored in the system. Every system queries a domain first for an IP address in its own system, so it is possible to redirect these requests to the localhost and prevent the pages from being loaded. It does not block anything in the narrower sense, but does not loading it at all.</p>")
        }
      }

      }

      ListItem.SingleValue {
      height: stevenColumn.childrenRect.height + units.gu(2)
      Column {
        id: stevenColumn
        anchors.fill: parent
        anchors.topMargin: units.gu(1)
        spacing: units.gu(2)
        Label {
          width: parent.width
          wrapMode: Text.Wrap
          horizontalAlignment: Text.AlignLeft
          text: i18n.tr("<b>Where do the lists come from?</b><p>The lists come from different projects like StevenBlack, Energized and AdGuard. At regular intervals, these lists are updated and anyone can use and collaborate for free. Already a large number of users have contributed to these lists.</p>")
        }
      }

      }
    }

  }
}
